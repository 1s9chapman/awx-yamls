This repo contains simple sample files needed to setup both the awx deployment, and the ingress

Start by applying the awx operator below.

kubectl apply -f https://raw.githubusercontent.com/ansible/awx-operator/<TAG>/deploy/awx-operator.yaml

Links for the latest version TAG are here:
https://github.com/ansible/awx-operator/releases

Then create the nginx ingress controller

helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

helm repo update

helm install ingress-nginx ingress-nginx/ingress-nginx

And finally apply the awx-demo.yml and awx-ingress.yml files

Apply the deployment first

kubectl apply -f awx-demo.yml

After a few mins apply the ingress

kubectl apply -f awx-ingress.yml

The operator and deployment are documented here:
https://github.com/ansible/awx-operator#basic-install

Linode process to setup an ingress documented here:
https://www.linode.com/docs/guides/how-to-deploy-nginx-ingress-on-linode-kubernetes-engine/

